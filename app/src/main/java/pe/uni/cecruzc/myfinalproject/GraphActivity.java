package pe.uni.cecruzc.myfinalproject;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.GridView;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.util.ArrayList;

public class GraphActivity extends AppCompatActivity {

    GridView graphGridView;
    ArrayList<String> textTitle = new ArrayList<>();

    GraphView graph;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graph);

        graph = findViewById(R.id.graph_view);
        graphGridView = findViewById(R.id.graph_grid_view);
        fillArray();

        GraphGridAdapter gridAdapter = new GraphGridAdapter(this, textTitle);
        graphGridView.setAdapter(gridAdapter);

        graphGridView.setOnItemClickListener((parent, view, position, id) -> {
            double x_start, x_end;
            int number_points;
            switch (position) {
                case 0:
                    x_start = -10.0;
                    x_end = 10.0;
                    number_points = 1000;
                    break;
                case 1:
                    x_start = -3.0;
                    x_end = 3.0;
                    number_points = 500;
                    break;
                case 2:
                case 3:
                    x_start = 0;
                    x_end = 2.0 * Math.PI;
                    number_points = 500;
                    break;
                default:
                    x_start = 0;
                    x_end = 0;
                    number_points = 1;
                    break;
            }
            drawGraph(position, x_start, x_end, number_points);
        });

        graph.setCursorMode(true);
    }

    private void fillArray() {
        textTitle.add("y = x");
        textTitle.add("y = x^2");
        textTitle.add("y = Sin(x)");
        textTitle.add("y = Cos(x)");
    }

    private void drawGraph(int option, double x_start, double x_end, int number_points) {
        LineGraphSeries<DataPoint> series = new LineGraphSeries<>();
        double y;
        double x_step = (x_end - x_start) / number_points;

        for(double x = x_start; x <= x_end; x += x_step) {
            switch (option) {
                case 0:
                    y = 1 * x;
                    break;
                case 1:
                    y = x * x;
                    break;
                case 2:
                    y = Math.sin(x);
                    break;
                case 3:
                    y = Math.cos(x);
                    break;
                default:
                    y = 0;
                    break;
            }
            series.appendData(new DataPoint(x, y), true, number_points);
        }
        graph.removeAllSeries();
        graph.addSeries(series);
    }
}