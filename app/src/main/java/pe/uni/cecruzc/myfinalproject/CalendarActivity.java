package pe.uni.cecruzc.myfinalproject;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class CalendarActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar);

        AlertDialog.Builder builder = new AlertDialog.Builder(CalendarActivity.this);
        builder
                .setTitle(R.string.calendar_title_dialog)
                .setMessage(R.string.calendar_msg_dialog)
                .setCancelable(false)
                .setPositiveButton(R.string.calendar_positive_dialog, (dialog, which) -> finish())
                .setNegativeButton(R.string.calendar_negative_dialog, (dialog, which) -> {
                    moveTaskToBack(true);
                    android.os.Process.killProcess(android.os.Process.myPid());
                    System.exit(0);
                })
                .create()
                .show();

    }
}