package pe.uni.cecruzc.myfinalproject;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.GridView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    GridView gridView;
    ArrayList<String> textTitle = new ArrayList<>();
    ArrayList<Integer> image = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        gridView = findViewById(R.id.grid_view);
        fillArray();

        GridAdapter gridAdapter = new GridAdapter(this, textTitle, image);
        gridView.setAdapter(gridAdapter);

        gridView.setOnItemClickListener((parent, view, position, id) -> {
            Intent intent = null;

            switch (position) {
                case 0:
                    intent = new Intent(MainActivity.this, CalculatorActivity.class);
                    break;
                case 1:
                    intent = new Intent(MainActivity.this, CalendarActivity.class);
                    break;
                case 2:
                    intent = new Intent(MainActivity.this, GraphActivity.class);
                    break;
                case 3:
                    intent = new Intent(MainActivity.this, ReadingActivity.class);
                    break;
                default:
                    break;
            }

            if(intent != null) {
                startActivity(intent);
            }

        });

    }

    private void fillArray() {
        textTitle.add("Calculadora");
        textTitle.add("Calendario");
        textTitle.add("Graficar funciones");
        textTitle.add("Leer números");

        image.add(R.drawable.calculator);
        image.add(R.drawable.calendar);
        image.add(R.drawable.chart);
        image.add(R.drawable.filled);
    }
}