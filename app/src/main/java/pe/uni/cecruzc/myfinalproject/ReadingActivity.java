package pe.uni.cecruzc.myfinalproject;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Locale;

public class ReadingActivity extends AppCompatActivity {

    EditText readingEditText;
    Button readingButton;
    TextToSpeech textToSpeech;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reading);

        readingEditText = findViewById(R.id.reading_edit_text);
        readingButton = findViewById(R.id.reading_button);

        textToSpeech = new TextToSpeech(ReadingActivity.this, status -> {
            if (status != TextToSpeech.ERROR) {
                textToSpeech.setLanguage(new Locale("es", "PE"));
            }
        });

        readingButton.setOnClickListener(v -> {
            String numberText = readingEditText.getText().toString();

            if(numberText.equals("")) {
                Toast.makeText(ReadingActivity.this, R.string.reading_empty_number_msg, Toast.LENGTH_SHORT).show();
                return;
            }

            if(textToSpeech == null) {
                Toast.makeText(ReadingActivity.this, R.string.reading_speaker_problem_msg, Toast.LENGTH_LONG).show();
            }
            else {
                textToSpeech.speak(numberText, TextToSpeech.QUEUE_FLUSH, null, "readingNumber");
            }
        });
    }
}