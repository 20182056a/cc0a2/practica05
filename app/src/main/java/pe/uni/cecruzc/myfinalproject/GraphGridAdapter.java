package pe.uni.cecruzc.myfinalproject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class GraphGridAdapter extends BaseAdapter {
    Context context;
    ArrayList<String> textTitle;

    public GraphGridAdapter(Context context, ArrayList<String> text_title) {
        this.context = context;
        this.textTitle = text_title;
    }

    @Override
    public int getCount() {
        return textTitle.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        @SuppressLint("ViewHolder") View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.graph_grid_view_layout, parent, false);

        TextView textTitleView = view.findViewById(R.id.text_view_option_graph);

        textTitleView.setText(textTitle.get(position));

        return view;
    }
}
