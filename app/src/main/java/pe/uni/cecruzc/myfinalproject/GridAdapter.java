package pe.uni.cecruzc.myfinalproject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class GridAdapter extends BaseAdapter {
    Context context;
    ArrayList<String> textTitle;
    ArrayList<Integer> image;

    public GridAdapter(Context context, ArrayList<String> text_title, ArrayList<Integer> image) {
        this.context = context;
        this.textTitle = text_title;
        this.image = image;
    }

    @Override
    public int getCount() {
        return textTitle.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        @SuppressLint("ViewHolder") View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_view_layout, parent, false);

        ImageView imageView = view.findViewById(R.id.image_view_option);
        TextView textTitleView = view.findViewById(R.id.text_view_option);

        imageView.setImageResource(image.get(position));
        textTitleView.setText(textTitle.get(position));

        return view;
    }

}
