package pe.uni.cecruzc.myfinalproject;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;

public class CalculatorActivity extends AppCompatActivity {

    final int MAX_LENGTH_NUMBER = 12;

    SharedPreferences sharedPreferences;

    LinearLayout historyLinearLayout, calculatorLinearLayout;
    TextView resultTextView, historyTextView;
    Button button1, button2, button3, button4, button5, button6, button7, button8, button9, button0;
    Button deleteButton, calculateButton, plusButton, minusButton, timesButton, divisionButton;
    Button showHistoryButton, clearHistoryButton;

    long accumulated = 0, input = 0, result = 0;
    Boolean isHistoryShown = false;
    char operation = '\0';

    String history = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculator);

        resultTextView = findViewById(R.id.result_text_view);
        button1 = findViewById(R.id.button_1);
        button2 = findViewById(R.id.button_2);
        button3 = findViewById(R.id.button_3);
        button4 = findViewById(R.id.button_4);
        button5 = findViewById(R.id.button_5);
        button6 = findViewById(R.id.button_6);
        button7 = findViewById(R.id.button_7);
        button8 = findViewById(R.id.button_8);
        button9 = findViewById(R.id.button_9);
        button0 = findViewById(R.id.button_0);
        deleteButton = findViewById(R.id.delete_button);
        calculateButton = findViewById(R.id.calculate_button);
        plusButton = findViewById(R.id.plus_button);
        minusButton = findViewById(R.id.minus_button);
        timesButton = findViewById(R.id.times_button);
        divisionButton = findViewById(R.id.division_button);
        showHistoryButton = findViewById(R.id.show_history_button);

        calculatorLinearLayout = findViewById(R.id.calculator_linear_layout);
        historyLinearLayout = findViewById(R.id.history_linear_layout);
        clearHistoryButton = findViewById(R.id.clear_history_button);
        historyTextView = findViewById(R.id.history_text_view);

        historyTextView.setMovementMethod(new ScrollingMovementMethod());

        plusButton.setOnClickListener(v -> setOperation('+'));
        minusButton.setOnClickListener(v -> setOperation('-'));
        timesButton.setOnClickListener(v -> setOperation('*'));
        divisionButton.setOnClickListener(v -> setOperation('/'));

        deleteButton.setOnClickListener(v -> deleteDigit());

        button0.setOnClickListener(v -> addDigit(0));
        button1.setOnClickListener(v -> addDigit(1));
        button2.setOnClickListener(v -> addDigit(2));
        button3.setOnClickListener(v -> addDigit(3));
        button4.setOnClickListener(v -> addDigit(4));
        button5.setOnClickListener(v -> addDigit(5));
        button6.setOnClickListener(v -> addDigit(6));
        button7.setOnClickListener(v -> addDigit(7));
        button8.setOnClickListener(v -> addDigit(8));
        button9.setOnClickListener(v -> addDigit(9));

        calculateButton.setOnClickListener(v -> {
            switch (operation){
                case '+':
                    result = accumulated + input;
                    break;
                case '-':
                    result = accumulated - input;
                    break;
                case '*':
                    result = accumulated * input;
                    break;
                case '/':
                    if(input == 0) {
                        Snackbar
                                .make(calculatorLinearLayout, R.string.msg_snack_bar, Snackbar.LENGTH_INDEFINITE)
                                .setAction(R.string.button_snack_bar_text, v1 -> {})
                                .show();
                        return;
                    }
                    result = accumulated / input;
                    break;
                default:
                    return;
            }

            history += (accumulated + " " + operation + " " + input);
            operation = '=';
            history += ("\n" + operation + " " + result + "\n");
            input = result;
            resultTextView.setText(String.valueOf(input));
            historyTextView.setGravity(Gravity.BOTTOM);
            historyTextView.setText(history);
            clearHistoryButton.setClickable(true);
            saveHistory();
        });

        showHistoryButton.setOnClickListener(v -> {
            if(isHistoryShown) {
                historyLinearLayout.setVisibility(View.GONE);
                showHistoryButton.setText(R.string.btn_show_history_calculator);
            }
            else {
                historyLinearLayout.setVisibility(View.VISIBLE);
                showHistoryButton.setText(R.string.btn_hide_history_calculator);
            }
            isHistoryShown = !isHistoryShown;
        });

        clearHistoryButton.setOnClickListener(v -> deleteHistory());

        retrieveHistory();
    }

    private void addDigit(int digit) {
        if(operation == '=') {
            input = 0;
            accumulated = 0;
            operation = '\0';
        }
        if(String.valueOf(input).length() == MAX_LENGTH_NUMBER) return;
        input *= 10;
        input += digit;
        resultTextView.setText(String.valueOf(input));
    }

    private void deleteDigit() {
        input /= 10;
        resultTextView.setText(String.valueOf(input));
    }

    private void setOperation(char op) {
        if(input != 0)  accumulated = input;
        operation = op;
        input = 0;
        resultTextView.setText(String.valueOf(input));
    }

    private void saveHistory() {
        sharedPreferences = getSharedPreferences("calculatorHistory", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("history", history);
        editor.apply();
    }

    private void retrieveHistory() {
        sharedPreferences = getSharedPreferences("calculatorHistory", Context.MODE_PRIVATE);
        history = sharedPreferences.getString("history", "");
        if(history.equals("")) {
            historyTextView.setText(getString(R.string.calculator_history_empty));
            historyTextView.setGravity(Gravity.CENTER_VERTICAL);
            clearHistoryButton.setClickable(false);
            return;
        }
        historyTextView.setText(history);
    }

    private void deleteHistory() {
        history = "";
        historyTextView.setText(getString(R.string.calculator_history_empty));
        historyTextView.setGravity(Gravity.CENTER_VERTICAL);
        saveHistory();
        clearHistoryButton.setClickable(false);
    }

}